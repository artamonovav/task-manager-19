package ru.t1.artamonov.tm.command.system;

import ru.t1.artamonov.tm.api.service.ICommandService;
import ru.t1.artamonov.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
