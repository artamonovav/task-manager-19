package ru.t1.artamonov.tm.api.repository;

import ru.t1.artamonov.tm.model.Project;

public interface IProjectRepository extends IRepository<Project> {

    void clear();

    Project create(String name, String description);

    Project create(String name);

}
